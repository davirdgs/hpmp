#include <iostream>
#include <fstream>
#include <math.h>
#include <string>
#include <vector>
#include <iostream>
#include <istream>
#include <ostream>
#include <iterator>
#include <sstream>
#include <stdlib.h>
#include <algorithm>
#include "Point.h"
#include "Instance.h"
#include <lemon/list_graph.h>
using namespace std;

// Constructor
Instance::Instance() {}

Instance::Instance(string filename, int covk) {
    coveragek = covk;
	ifstream instanceFile(filename);

	string line;
	bool waitSize = false;
	bool waitPoint = false;

	if (instanceFile.is_open()) {

		while (getline(instanceFile,line)) {

			stringstream strstr(line);

            // use stream iterators to copy the stream to the vector as whitespace separated strings
			istream_iterator<string> it(strstr);
			istream_iterator<string> end;
			vector<string> results(it, end);

			if(waitPoint) {
				if(results[0].compare("EOF") == 0) {
					break;
				}
				int x = int(stof(results[1]));
				int y = int(stof(results[2]));

                Point point = Point(x, y);
                points.push_back(point);
			}

			for (auto a : results) {

				if(waitSize) {
					size = stoi(a);
					waitSize = false;
				}

				if(a.compare("DIMENSION:") == 0) {
					waitSize = true;
				}

				if(a.compare("NODE_COORD_SECTION") == 0) {
					waitPoint = true;
				}
			}
		}

		instanceFile.close();

	} else cout << "Unable to open file";

	instanceFile.close();
    setDistMatrix();
    setCoverageD(coveragek);
    setCoverageV();
    //printCoverage();
}

void Instance::setDistMatrix() {
    for(int i = 0; i < points.size(); i++) {
        
        distMatrix.push_back(vector<double>());
        
        for(int j = 0; j < points.size(); j++) {
            distMatrix[i].push_back(euclidianDistance(points[i], points[j]));
        }
    }
}

// Nós cobertos pelo ponto
void Instance::setCoverageD(int ck) {
    std::vector<int>::iterator it;
    for(int i = 0; i < points.size(); i++) {
        // Todo nó cobre a si mesmo
        points[i].coverageD.push_back(i);
        for(int j = 0; j < points.size(); j++) {
            if(i == j) continue;
            double dist_ij = distMatrix[i][j];
            // Insere ordenado
            for(int k = 0; k < points[i].coverageD.size(); k++) {
                // Se dist maior, insere depois
                if(dist_ij > distMatrix[i][points[i].coverageD[k]]) {
                    if(k == points[i].coverageD.size() - 1) {
                        it = points[i].coverageD.end();
                        points[i].coverageD.insert(it, j);
                        break;
                    }

                    if(dist_ij < distMatrix[i][points[i].coverageD[k + 1]]) {
                        it = points[i].coverageD.begin() + k + 1;
                        points[i].coverageD.insert(it, j);
                        break;
                    }
                }
            }
        }
        points[i].coverageD.resize(ck);
    }
}

// Nós que cobrem o ponto
void Instance::setCoverageV() {
    for(int p = 0; p < points.size(); p++) {
        for(int i = 0; i < points.size(); i++) {
            for(int j = 0; j < points[i].coverageD.size(); j++) {
                if(p == points[i].coverageD[j])
                    points[p].coverageV.push_back(i);
            }
        }
    }
}

void Instance::printCoverage() {
    cout << endl << "Print coverage D" << endl;
    for(int i = 0; i < points.size(); i++) {
        cout << i + 1 << ": ";
        for(int k = 0; k < points[i].coverageD.size(); k++) {
            cout << points[i].coverageD[k] + 1 << " ";
        }
        cout << endl;
    }

    cout << endl << endl;
    cout << endl << "Print coverage V" << endl;
    for(int i = 0; i < points.size(); i++) {
        cout << i + 1 << ": ";
        for(int k = 0; k < points[i].coverageV.size(); k++) {
            cout << points[i].coverageV[k] + 1 << " ";
        }
        cout << endl;
    }
    cout << "End print coverage" << endl << endl;
}
