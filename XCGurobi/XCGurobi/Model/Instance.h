#ifndef INSTANCE_H
#define INSTANCE_H

#include <vector>
#include "Point.h"
#include <lemon/list_graph.h>

using namespace std;

class Instance {
public:
	int size;
	vector<Point> points;
	vector<vector<double>> distMatrix;
	Instance(string filename, int covk);
    Instance();
    int coveragek;
    void setDistMatrix();
    void setCoverageD(int ck);
    void setCoverageV();
    void printCoverage();
};

#endif
