#include <iostream>
#include <math.h>
#include "Point.h"
using namespace std;

Point::Point(int xValue, int yValue) {
    coverageD = std::vector<int>();
    coverageV = std::vector<int>();
    x = xValue;
    y = yValue;
}

double euclidianDistance(Point a, Point b) {
    double xa = double(a.x);
    double ya = double(a.y);
    double xb = double(b.x);
    double yb = double(b.y);
    return sqrt((xa - xb)*(xa - xb) + (ya - yb)*(ya - yb));
    //return ceil(sqrt((xa - xb)*(xa - xb) + (ya - yb)*(ya - yb)));
}
