//
//  solution.hpp
//  XCHpmp
//
//  Created by Davi Rodrigues on 04/06/19.
//

#ifndef solution_hpp
#define solution_hpp

#include <stdio.h>
#include <vector>
#include "Instance.h"
#include "chromosome.hpp"

using namespace std;
using namespace BRKGA;

const unsigned KMEANS = 0;
const unsigned GIANT_TOUR = 1;
const unsigned CHR_GIANT_TOUR = 2;

class Solution {
public:
    vector<vector<int>> sol;
    int nCycles;
    int instanceSize;
    void greedHeuristic(Instance instance);
    void buildFromChromosome(Instance instance, Chromosome& chromosome);
    void giantTourHeuristics(unsigned heuristic, bool skipHeuristic);
    void kmeansHeuristic(Instance instance);
    double evaluateSolution(Instance instance);
    int getGki(int k, int i, vector<int> hCycle);
    double getCij(int start, int end);
    void printSolution(Instance instance);
    void writeInFile(string path);
    void fixSolution(Instance instance);
    Solution(Instance instance, int cycles, unsigned heuristic);
    Solution(Instance instance, int cycles, unsigned heuristic, Chromosome& chromosome);
    Solution();
};

#endif /* solution_hpp */
