//
//  solution.cpp
//  XCHpmp
//
//  Created by Davi Rodrigues on 04/06/19.
//

#include <iostream>
#include <fstream>
#include <float.h>
#include "Solution.hpp"
using namespace std;

Solution::Solution() {}

const int _INFINITY = -2;
const double UNDEFINED = -1;
Instance GIANT_TOUR_INSTANCE;
vector<vector<double>> MEMOIZATION;
Chromosome solChr;

Solution::Solution(Instance instance, int cycles, unsigned heuristic) {
    nCycles = cycles;
    instanceSize = instance.size;

    for(int i = 0; i < nCycles; i++) {
        sol.push_back(vector<int>());
    }

    if(heuristic == KMEANS) {
        kmeansHeuristic(instance);
    } else if(heuristic == GIANT_TOUR) {
        GIANT_TOUR_INSTANCE = instance;
        MEMOIZATION.clear();
        giantTourHeuristics(heuristic, false);
    }
}

Solution::Solution(Instance instance, int cycles, unsigned heuristic, Chromosome& chromosome) {
    nCycles = cycles;
    instanceSize = instance.size;
    solChr = chromosome;
    
    for(int i = 0; i < nCycles; i++) {
        sol.push_back(vector<int>());
    }
    
    if (heuristic == KMEANS) {
        kmeansHeuristic(instance);
    } else if (heuristic == GIANT_TOUR || heuristic == CHR_GIANT_TOUR) {
        GIANT_TOUR_INSTANCE = instance;
        MEMOIZATION.clear();
        giantTourHeuristics(heuristic, false);
    }
}

void Solution::buildFromChromosome(Instance instance, Chromosome& chromosome) {
    vector<pair<double, unsigned>> permutation(instance.size);
    for(unsigned i = 0; i < instance.size; ++i)
        permutation[i] = make_pair(chromosome[i], i);

    sort(permutation.begin(), permutation.end());
    for(unsigned i = 0; i < instance.size; ++i) {
        sol[0].push_back(permutation[i].second);
    }
}

void Solution::greedHeuristic(Instance instance) {
    vector<bool> usedIndex = vector<bool>();
    for(int i = 0; i < instanceSize; i++) {
        usedIndex.push_back(false);
    }
    
    sol[0].push_back(0);
    usedIndex[0] = true;
    
    for(int i = 0; i < instanceSize; i++) {
        
        int next = INT_MAX;
        double bestDist = DBL_MAX;
        for(int j = 0; j < instanceSize; j++) {
            if(usedIndex[j] || i == j)
                continue;
            int last = sol[0][sol[0].size()-1];
            double dist = instance.distMatrix[last][j];
            if(dist < bestDist) {
                bestDist = dist;
                next = j;
            }
        }
        
        if(next <= instance.size) {
            sol[0].push_back(next);
            usedIndex[next] = true;
        }
    }
}

void Solution::giantTourHeuristics(unsigned heuristic, bool skipHeuristic) {
    if(!skipHeuristic) {
        if(heuristic == GIANT_TOUR) {
            greedHeuristic(GIANT_TOUR_INSTANCE);
        } else if(heuristic == CHR_GIANT_TOUR) {
            buildFromChromosome(GIANT_TOUR_INSTANCE, solChr);
        }
    }
    vector<int> greedCopy = sol[0];
    int bestG0 = INT_MAX;

    for(int rot = 0; rot < greedCopy.size(); rot++) { // Remove for BRKGA

        for(int i = 0; i < nCycles; i++) {
            MEMOIZATION.push_back(vector<double>());
            for(int j = 0; j < GIANT_TOUR_INSTANCE.size; j++) {
                MEMOIZATION[i].push_back(UNDEFINED);
            }
        }

        int g00 = getGki(0, 0, greedCopy);
        if(g00 >= bestG0) {
            int first = greedCopy[0];
            greedCopy.push_back(first);
            greedCopy.erase(greedCopy.begin());
            continue; // remove for BRKGA
        }
        bestG0 = g00;

        int cycleStart = 0;
        vector<int> cycle;

        for(int k = 0; k < nCycles; k++) {
            double cycleCost = 0;
            double cycleTarget = MEMOIZATION[k][cycleStart];

            for(int i = cycleStart + 1; i < GIANT_TOUR_INSTANCE.size; i++) {
                if(k == nCycles - 1)
                    break;
                cycleCost  = getCij(cycleStart, i);
                if(cycleCost + MEMOIZATION[k+1][i+1] + 1 > cycleTarget) {
                    sol[k].clear();
                    sol[k] = vector<int>(greedCopy.begin() + cycleStart, greedCopy.begin() + i + 1);
                    cycleStart = i + 1;
                    break;
                }
            }
        }

        sol[sol.size() - 1].clear();
        sol[sol.size() - 1] = vector<int>(greedCopy.begin() + cycleStart, greedCopy.end());
        cout<< "";
    } // remove for BRKGA
}

int Solution::getGki(int k, int i, vector<int> hCycle) {

    if(MEMOIZATION[k][i] != UNDEFINED)
        return MEMOIZATION[k][i];

    if(k == (nCycles - 1) && 3*k <= i && i <= (instanceSize - 3)) { // last cycle
        MEMOIZATION[k][i] = getCij(i, instanceSize - 1);
        return MEMOIZATION[k][i];

    } else if(k < (nCycles - 1) && 3*k <= i && i <= (instanceSize - 3*(nCycles - k))) {
        int min = _INFINITY;
        int jIndex = UNDEFINED;
        for(int j = (i + 2); j <= instanceSize - 3*(nCycles - k) + 2; j++) {
            int next = getGki(k+1, j+1, hCycle) + getCij(i, j);
            if(next < min || min == _INFINITY)
                min = next;
            jIndex = j;
        }
        assert(jIndex != UNDEFINED);
        MEMOIZATION[k][i] = min;
        return MEMOIZATION[k][i];

    } else {
        MEMOIZATION[k][i] = _INFINITY;
        return MEMOIZATION[k][i];
    }
}

double Solution::getCij(int start, int end) {
    double cin = 0;

    for(int i = start; i < end; i++) {
        cin = cin + GIANT_TOUR_INSTANCE.distMatrix[i+1][i];
    }
    cin = cin + GIANT_TOUR_INSTANCE.distMatrix[start][end];
    return cin;
}

void assignPoints(Instance instance, vector<int> &centroids, vector<vector<int>> &clusters) {
    // Assign points
    
    for(int i = 0; i < instance.points.size(); i++) {
        
        if(find(centroids.begin(), centroids.end(), i) != centroids.end())
            continue;
        
        int bestClusterIndex = -1;
        double bestDist = DBL_MAX;
        int pointIndex = -1;
        
        for(int j = 0; j < clusters.size(); j++) {
            double distToCluster = instance.distMatrix[i][clusters[j][0]];
            if(distToCluster < bestDist) {
                bestDist = distToCluster;
                bestClusterIndex = j;
                pointIndex = i;
            }
        }
        assert(bestClusterIndex != -1);
        assert(pointIndex != -1);
        clusters[bestClusterIndex].push_back(pointIndex);
    }
}

void Solution::fixSolution(Instance instance) {
    vector<int> plainSol = vector<int>();
    for(int i = 0; i < nCycles; i++) {
        plainSol.insert(plainSol.end(), sol[i].begin(), sol[i].end());
        sol[i].clear();
    }
    GIANT_TOUR_INSTANCE = instance;
    MEMOIZATION.clear();
    sol[0] = plainSol;
    giantTourHeuristics(GIANT_TOUR, true);
}

void Solution::kmeansHeuristic(Instance instance) {
    vector<int> centroids;
    vector<vector<int>> clusters;
    
    srand((uint)time(0)); // seeds rand
    
    //int iterations = 50;
    
    // Random centroids
    for(int i = 0; i < nCycles; i++) {
        int rCent = rand() % instanceSize;
        if(find(centroids.begin(), centroids.end(), rCent) == centroids.end()) {
            centroids.push_back(rCent);
            clusters.push_back(vector<int>());
            // Centroid as head
            clusters[i].push_back(rCent);
        } else {
            i--;
        }
    }

    int max_time_ms = 180 * 1000;
    auto start = chrono::system_clock::now();
    auto end = chrono::system_clock::now();
    int itCount = 0;
    while(chrono::duration_cast <chrono::milliseconds>(end - start).count() < max_time_ms) {
    //for(int k = 0; k < iterations; k++) {
        
        assignPoints(instance, centroids, clusters);
        
        // Recalculate centroids
        
        for(int j = 0; j < clusters.size(); j++) {
            
            int xValue = 0;
            int yValue = 0;
            
            for(int i : clusters[j]) {
                xValue += instance.points[i].x;
                yValue += instance.points[i].y;
            }
            
            xValue = xValue / clusters[j].size();
            yValue = yValue / clusters[j].size();
            
            Point centroid = Point(xValue, yValue);
            
            // Find closest point
            int centoidIndex = -1;
            double minCentoidDist = DBL_MAX;
            
            for(int i : clusters[j]) {
                int newDist = euclidianDistance(instance.points[i], centroid);
                if(newDist < minCentoidDist) {
                    minCentoidDist = newDist;
                    centoidIndex = i;
                }
            }

            assert(centoidIndex >= 0);

            if(centroids[j] != centoidIndex) {
                itCount = 0;
            }
            centroids[j] = centoidIndex;
            clusters[j].clear();
            clusters[j].push_back(centoidIndex);
            
        } // for cluster : clusters
        if(itCount >= 100) {
            cout << "100 iterações sem melhoria" << endl;
            break;
        }
        itCount++;
        end = chrono::system_clock::now();
    } // for iterations
    
    assignPoints(instance, centroids, clusters);
    
    // Populate sol
    for(int i = 0; i < nCycles; i++) {
        sol[i] = clusters[i];
    }
    fixSolution(instance);
}

double Solution::evaluateSolution(Instance instance) {
    double totalCost = 0;
    
    for(int i = 0; i < nCycles; i++) {
        if(sol[i].size() < 2)
            continue;
        for(int j = 1; j < sol[i].size(); j++) {
            totalCost = totalCost + instance.distMatrix[sol[i][j]][sol[i][j-1]];
        }
        totalCost = totalCost + instance.distMatrix[sol[i][0]][sol[i][sol[i].size()-1]];
    }
    
    return totalCost;
}

void Solution::printSolution(Instance instance) {
    cout << nCycles << " ciclos" << endl << "Tamanho da instancia: " << instanceSize << endl;
    cout << "Custo: " << evaluateSolution(instance) << endl;
    
    for(int k = 0; k < sol.size(); k++) {
        cout << "Ciclo " << k << endl;
        for (int i = 0; i < sol[k].size(); i++) {
            cout << sol[k][i] << " ";
        }
        cout << endl;
    }
}

void Solution::writeInFile(string path) {
    ofstream file;
    file.open(path);
    for(int k = 0; k < sol.size(); k++) {
        for (int i = 0; i < sol[k].size(); i++) {
            file << sol[k][i] << " ";
        }
        file << "\n";
    }
    file.close();
}
