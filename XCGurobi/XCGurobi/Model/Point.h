#ifndef Point_h
#define Point_h

#include <vector>

class Point {
public:
    int x;
    int y;
    // Nós cobertos pelo ponto
    std::vector<int> coverageD;
    // Nós que cobrem o ponto
    std::vector<int> coverageV;
    Point(int xValue, int yValue);
};

double euclidianDistance(Point a, Point b);

#endif
