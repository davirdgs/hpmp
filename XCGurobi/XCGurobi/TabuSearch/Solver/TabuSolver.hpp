//
//  TabuSolver.hpp
//  XCHpmp
//
//  Created by Davi Rodrigues on 08/06/19.
//

#ifndef TabuSolver_hpp
#define TabuSolver_hpp

#include <stdio.h>
#include <vector>
#include "TabuMove.hpp"
#include "Operations.hpp"

const int INTENSIFICATION = 0;
const int DIVERSIFICATION = 1;
const int REACTIVE_SEARCH = 2;
const int PROBABILISTIC_SEARCH = 3;
const int USE_FIRST_IMPROVEMENT = 4;
const int ALLOW_INFACTIBILITY = 5;

class TabuSolver {
public:
    std::vector<TabuMove> candidateList;
    std::vector<int> tabuList;
    std::vector<int> frequencyList;
    int tenure;
    double incubentCost;
    double bestCost;
    int itCount;
    int itControl;

    // parameters
    bool intensification;
    bool diversification;
    bool reactiveSearch;
    bool probabilisticSearch;
    bool useFirstImproviment; // best as default
    bool allowInfactibity;

    void tabuIntensification();
    void tabuDiversification();
    void resetLists();

    Solution bestSol;
    Solution incubentSolution;
    Instance instance;
    TabuSolver(int _tenure, Instance _instance, Solution startSol, vector<int> parameters);
    void tabuSolver(Instance instance, long int seconds);
    void tabuNeighborMove();
    void performMove(TabuMove move, Instance &instance);
    void fixSolution();
    TabuMove getBestMove();
};

#endif /* TabuSolver_hpp */
