//
//  TabuSolver.cpp
//  XCHpmp
//
//  Created by Davi Rodrigues on 08/06/19.
//

#include "TabuSolver.hpp"
#include <chrono>
#include <iostream>

using namespace std;

TabuSolver::TabuSolver(int _tenure, Instance _instance, Solution startSol, vector<int> parameters) {
    assert(_tenure > 0);
    tenure = _tenure;
    itCount = 0;
    itControl = 0;
    instance = _instance;
    startSol.fixSolution(instance);
    bestSol = startSol;
    incubentSolution = startSol;
    incubentCost = incubentSolution.evaluateSolution(_instance);
    bestCost = incubentCost;

    cout << "Reseting lists" << endl;
    resetLists();

    // Parameters
    cout << "Parsing parameters" << endl;
    intensification = find(parameters.begin(), parameters.end(), INTENSIFICATION) != parameters.end();
    diversification = find(parameters.begin(), parameters.end(), DIVERSIFICATION) != parameters.end();
    reactiveSearch = find(parameters.begin(), parameters.end(), REACTIVE_SEARCH) != parameters.end();
    probabilisticSearch = find(parameters.begin(), parameters.end(), PROBABILISTIC_SEARCH) != parameters.end();
    useFirstImproviment = find(parameters.begin(), parameters.end(), USE_FIRST_IMPROVEMENT) != parameters.end();
    allowInfactibity = find(parameters.begin(), parameters.end(), ALLOW_INFACTIBILITY) != parameters.end();
}

void TabuSolver::tabuSolver(Instance instance, long int seconds) {

    auto start = chrono::system_clock::now();
    auto end = chrono::system_clock::now();
    int lastImproviment = 0;
    double lastBestCost = bestCost;

    while(chrono::duration_cast < chrono::seconds>(end - start).count() < seconds) {
        //cout << "\nitCount: " + to_string(itCount) << endl;
        candidateList.clear();
        tabuNeighborMove(); // Populate candidate list with viable moves
        TabuMove nextMove = getBestMove();
        performMove(nextMove, instance);
        itCount++;
        itControl++;
        if(itControl >= 20) {
            itControl = 0;
            if(intensification)
                tabuIntensification();
            if(diversification)
                tabuDiversification();
        }

        //if(allowInfactibity)
            fixSolution();

        if(lastBestCost != bestCost) {
            lastBestCost = bestCost;
            lastImproviment = 0;
        }
        lastImproviment++;
        //if(lastImproviment >= 100) {
        //    break;
        //}
        end = chrono::system_clock::now();
    }
    cout << "\nitCount: " + to_string(itCount) << endl;

}

void TabuSolver::tabuNeighborMove() {
    int prob = 0;

    for(int i = 0; i < incubentSolution.sol.size(); i++) {
        for(int j = 0; j < incubentSolution.sol[i].size(); j++) {
            // first elem
            int elemA = incubentSolution.sol[i][j];
            if((find(tabuList.begin(), tabuList.end(), elemA)) != tabuList.end())
                continue;

            for(int k = i; k < incubentSolution.sol.size(); k++) {
                for(int l = 0; l < incubentSolution.sol[k].size(); l++) {
                    // Probabilistic
                    if(probabilisticSearch) {
                        srand((unsigned)time(0) + prob);
                        prob = rand() % 100;
                        if(prob < 40 && !candidateList.empty())
                            continue;
                    }

                    // second elem
                    int elemB = incubentSolution.sol[k][l];
                    if((find(tabuList.begin(), tabuList.end(), elemA)) != tabuList.end())
                        continue;
                    if(elemA == elemB)
                        continue;

                    //cout << "i: " << i << " j: " << j << " k: " << k << " l: " << l << "\n";

                    // 2-exchange moves
                    TabuMove twoExMove = TabuMove();
                    Solution twoExSol = incubentSolution;
                    twoExchange(twoExSol, i, k, j, l);
                    twoExMove.solution = twoExSol;
                    twoExMove.solutionCost = twoExSol.evaluateSolution(instance);
                    twoExMove.moveType = TWO_EXCHANGE_MOVE;
                    twoExMove.tabuElemA = elemA;
                    twoExMove.tabuElemB = elemB;
                    candidateList.push_back(twoExMove);

                    // 1-exchange moves
                    TabuMove oneExMove = TabuMove();
                    Solution oneExSol = incubentSolution;
                    if (oneExchange(oneExSol, i, k, j, l, allowInfactibity)) { // Cut invalid moves
                        oneExMove.solution = oneExSol;
                        oneExMove.solutionCost = oneExSol.evaluateSolution(instance);
                        oneExMove.moveType = ONE_EXCHANGE_MOVE;
                        oneExMove.tabuElemA = elemA;
                        oneExMove.tabuElemB = elemB;
                        candidateList.push_back(oneExMove);
                        //Aspiration criteria
                        if(oneExMove.solutionCost < bestCost) {
                            candidateList.clear();
                            candidateList.push_back(oneExMove);
                            return;
                        }
                    }

                    //Aspiration criteria
                    if(twoExMove.solutionCost < bestCost) {
                        candidateList.clear();
                        candidateList.push_back(twoExMove);
                        return;
                    }
                }
            }
        }
    }
    cout<<"";
}

void TabuSolver::performMove(TabuMove move, Instance &instance) {
    incubentSolution = move.solution;
    incubentCost = move.solutionCost;

    if(incubentCost < bestCost) {
        bestSol = move.solution;
        bestCost = move.solutionCost;
        cout << " Best cost: " << bestCost << " Iteration: " << itCount << endl;
        itControl = 0;
        while(tabuList.size() > tenure) {
            tabuList.erase(tabuList.begin());
        }
    } else if(itControl % 20 == 0 && reactiveSearch && tabuList.size() < instance.size / 2) {
        tabuList.push_back(-1);
    }

    tabuList.erase(tabuList.begin());
    tabuList.push_back(move.tabuElemA);
    frequencyList[move.tabuElemA] = frequencyList[move.tabuElemA] + 1;
    if(move.moveType == TWO_EXCHANGE_MOVE) {
        tabuList.erase(tabuList.begin());
        tabuList.push_back(move.tabuElemB);
        frequencyList[move.tabuElemB] = frequencyList[move.tabuElemB] + 1;
    }
}

void TabuSolver::resetLists() {
    frequencyList.clear();
    tabuList.clear();
    for(int i = 0; i < instance.size; i++) {
        frequencyList.push_back(0);
    }

    for(int i = 0; i < tenure; i++) {
        tabuList.push_back(-1);
    }
}

void TabuSolver::tabuIntensification() {
    // Get 20% most frequents elem in tabu list
    vector<int> intensificationElem;
    for(int i = 0; i < frequencyList.size()/5; i++) {
        int max = 0;
        int maxIndex = -1;
        for(int j = 0; j < frequencyList.size(); j++) {
            if(frequencyList[j] > max && find(intensificationElem.begin(), intensificationElem.end(), j) == intensificationElem.end()) {
                max = frequencyList[j];
                maxIndex = j;
            }
        }
        intensificationElem.push_back(maxIndex);
    }

    // Random 1-exchange in intensificationElem
    int seed = 0;
    for(int i = 0; i < intensificationElem.size(); i++) {
        srand((unsigned)time(0) + seed);
        seed = rand() % intensificationElem.size();
        int sortedElem = intensificationElem[seed];

        // Find sorted elem
        for(int cycle = 0; cycle < incubentSolution.sol.size(); cycle++) {
            for(int position = 0; position < incubentSolution.sol[cycle].size(); position++) {
                if(incubentSolution.sol[cycle][position] == sortedElem) {
                    srand((unsigned)time(0) + seed);
                    seed = rand() % incubentSolution.sol.size();
                    int randCycle = seed;
                    srand((unsigned)time(0) + seed);
                    seed = rand() % incubentSolution.sol[randCycle].size();
                    int randPosition = seed;
                    oneExchange(incubentSolution, cycle, randCycle, position, randPosition, false);
                }
            }
        }
    }
    resetLists();
}

void TabuSolver::tabuDiversification() {
    // Get 20% least frequents elem in tabu list
    vector<int> diversificationElem;
    for(int i = 0; i < frequencyList.size()/5; i++) {
        int min = INT_MAX;
        int minIndex = -1;
        for(int j = 0; j < frequencyList.size(); j++) {
            if(frequencyList[j] < min && find(diversificationElem.begin(), diversificationElem.end(), j) == diversificationElem.end()) {
                min = frequencyList[j];
                minIndex = j;
            }
        }
        diversificationElem.push_back(minIndex);
    }

    // Random 1-exchange in diversificationElem
    int seed = 0;
    for(int i = 0; i < diversificationElem.size(); i++) {
        srand((unsigned)time(0) + seed);
        seed = rand() % diversificationElem.size();
        int sortedElem = diversificationElem[seed];

        // Find sorted elem
        for(int cycle = 0; cycle < incubentSolution.sol.size(); cycle++) {
            for(int position = 0; position < incubentSolution.sol[cycle].size(); position++) {
                if(incubentSolution.sol[cycle][position] == sortedElem) {
                    srand((unsigned)time(0) + seed);
                    seed = rand() % incubentSolution.sol.size();
                    int randCycle = seed;
                    srand((unsigned)time(0) + seed);
                    seed = rand() % incubentSolution.sol[randCycle].size();
                    int randPosition = seed;
                    oneExchange(incubentSolution, cycle, randCycle, position, randPosition, false);
                }
            }
        }
    }
    resetLists();
}

void TabuSolver::fixSolution() {
    for(int i = 0; i < incubentSolution.sol.size(); i++) {
        if(incubentSolution.sol[i].size() < 3) {
            // fix solution
            candidateList.clear();
            // populate candidate list
            for(int toPosition = 0; toPosition < incubentSolution.sol[i].size(); toPosition++) {
                for(int fromCycle = 0; fromCycle < incubentSolution.sol.size(); fromCycle++) {
                    if(i == fromCycle)
                        continue;
                    for(int elem = 0; elem < incubentSolution.sol[fromCycle].size(); elem++) {
                        if((find(tabuList.begin(), tabuList.end(), elem)) != tabuList.end())
                            continue;

                        // 1-exchange moves
                        TabuMove oneExMove = TabuMove();
                        Solution oneExSol = incubentSolution;
                        if (oneExchange(oneExSol, fromCycle, i, elem, toPosition, false)) {
                            oneExMove.solution = oneExSol;
                            oneExMove.solutionCost = oneExSol.evaluateSolution(instance);
                            oneExMove.moveType = ONE_EXCHANGE_MOVE;
                            oneExMove.tabuElemA = elem;
                            oneExMove.tabuElemB = elem;
                            candidateList.push_back(oneExMove);
                        }
                    }
                }
            }

            // get best move
            TabuMove move = getBestMove();
            performMove(move, instance);
            i--;
        }
    }
}

TabuMove TabuSolver::getBestMove() {
    if(candidateList.empty()) {
        tabuNeighborMove();
    }
    assert(!candidateList.empty());
    TabuMove bestMove;
    int bestCost = INT_MAX;
    bool setted = false;

    for(TabuMove move : candidateList) {
        if(move.solutionCost < bestCost) {
            bestCost = move.solutionCost;
            bestMove = move;
            setted = true;

            // First improviment
            if(useFirstImproviment && move.solutionCost < incubentCost) {
                return move;
            }
        }
    }
    assert(setted);
    return bestMove;
}
