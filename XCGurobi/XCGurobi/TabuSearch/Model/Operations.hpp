//
//  operations.hpp
//  XCHpmp
//
//  Created by Davi Rodrigues on 06/06/19.
//

#ifndef operations_hpp
#define operations_hpp

#include <stdio.h>
#include "Solution.hpp"

bool oneExchange(Solution &solution, int fromCycle, int toCycle, int itemPosition, int toPosition, bool invalidMoves);
void twoExchange(Solution &solution, int cycleA, int cycleB, int positionA, int positionB);

#endif /* operations_hpp */
