//
//  TabuMove.hpp
//  XCHpmp
//
//  Created by Davi Rodrigues on 08/06/19.
//

#ifndef TabuMove_hpp
#define TabuMove_hpp

#include <stdio.h>
#include "Solution.hpp"

const int ONE_EXCHANGE_MOVE = 1;
const int TWO_EXCHANGE_MOVE = 2;

class TabuMove {
    
public:
    Solution solution;
    double solutionCost;
    int tabuElemA;
    int tabuElemB;
    int moveType;
    TabuMove();
};

#endif /* TabuMove_hpp */
