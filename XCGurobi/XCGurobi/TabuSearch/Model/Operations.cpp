//
//  operations.cpp
//  XCHpmp
//
//  Created by Davi Rodrigues on 06/06/19.
//

#include "Operations.hpp"

bool oneExchange(Solution &solution, int fromCycle, int toCycle, int itemPosition, int toPosition, bool invalidMoves) {
    int item = solution.sol[fromCycle][itemPosition];

    if(solution.sol[fromCycle].size() <= 4)
        return false;

    solution.sol[fromCycle].erase(solution.sol[fromCycle].begin() + itemPosition);
    solution.sol[toCycle].insert(solution.sol[toCycle].begin() + toPosition, item);
    return true;
}

void twoExchange(Solution &solution, int cycleA, int cycleB, int positionA, int positionB) {
    int itemA = solution.sol[cycleA][positionA];
    int itemB = solution.sol[cycleB][positionB];
    solution.sol[cycleA][positionA] = itemB;
    solution.sol[cycleB][positionB] = itemA;
}
