#include <iostream>
#include <math.h>
#include <string>
#include <cassert>
#include <cstdlib>
#include <cmath>
#include <float.h>
#include "Point.h"
#include "Instance.h"
#include "GurobiSolver.hpp"
#include "gurobi_c++.h"
#include "Solution.hpp"
#include "TabuSolver.hpp"
#include "brkga_mp_ipr.hpp"
#include "HpMPDecoder.hpp"
#include "Config.hpp"

using namespace std;

string _itos(int i) {stringstream s; s << i; return s.str(); }

Solution perforBRKGA(string instance_file, int max_time_s, int cycles) {
    try {
        const unsigned seed = 27000001;

        cout << "Reading data..." << endl;
        auto instance = Instance(instance_file, 4);

        cout << "Reading parameters..." << endl;

        cout << instance.size << endl;
        auto params = BRKGA::readConfiguration(brkga_config_file);
        auto& brkga_params = params.first;

        cout << "Building BRKGA data and initializing..." << endl;

        HpMPDecoder decoder(instance, cycles);

        BRKGA::BRKGA_MP_IPR<HpMPDecoder> algorithm(decoder, BRKGA::Sense::MINIMIZE, seed,
                                                   instance.size, brkga_params);

        algorithm.initialize();
        double best_cost = DBL_MAX;
        int max_time_ms = max_time_s * 1000;
        int generations = 0;
        int currentTime = -1;
        auto start = chrono::system_clock::now();
        auto end = chrono::system_clock::now();
        while(chrono::duration_cast <chrono::milliseconds>(end - start).count() < max_time_ms) {
            generations++;
            algorithm.evolve();
            auto fitness = algorithm.getBestFitness();
            end = chrono::system_clock::now();
            if(fitness < best_cost) {
                best_cost = fitness;
                cout << "best_cost: " << best_cost << endl;
                cout << "generations: " << generations << endl;
            }
            double cTime = chrono::duration_cast <chrono::milliseconds>(end - start).count()/60000;
            if (cTime > currentTime) {
                cout << "current_time: " << cTime << endl;
                currentTime = cTime;
            }
        }

        cout << "n generations: " << generations << endl;
        Chromosome best = algorithm.getBestChromosome();
        Solution bestSol = Solution(instance, cycles, CHR_GIANT_TOUR, best);
        //bestSol.printSolution(instance);
        return bestSol;
    }
    catch(exception& e) {
        cerr << "\n***********************************************************"
             << "\n****  Exception Occured: " << e.what()
             << "\n***********************************************************"
             << endl;
        return Solution();
    }
}

Solution tabuSearch(int tenure, Instance instance, int cycles) {
    auto start = chrono::system_clock::now();

    Solution sol = Solution(instance, cycles, KMEANS);

    vector<int> parameters;
//   parameters.push_back(INTENSIFICATION);
    parameters.push_back(DIVERSIFICATION);
//    parameters.push_back(REACTIVE_SEARCH);
//    parameters.push_back(PROBABILISTIC_SEARCH);
    parameters.push_back(USE_FIRST_IMPROVEMENT);
    parameters.push_back(ALLOW_INFACTIBILITY);
    TabuSolver solver = TabuSolver(tenure, instance, sol, parameters);
    solver.tabuSolver(instance, 3600);

    cout << solver.bestCost << endl;
    auto end = chrono::system_clock::now();
    cout << chrono::duration_cast < chrono::milliseconds>(end - start).count() << endl;
    return solver.bestSol;
}

Solution coverageSolution(Solution sol, Instance instance) {

    for(int p = 0; p < sol.sol.size(); p++) {
        for(int i = 0; i < sol.sol[p].size(); i++) {
            // For each node, check if it is covered by another node in the solution

            int no = sol.sol[p][i];
            bool toRemove = false;

            for(int c = 0; c < instance.points[no].coverageD.size(); c++) {
                int uncovered = instance.points[no].coverageD[c];
                // search if uncovered node is covered by another node in the solution

                bool covered = false;
                for(int k = 0; k < sol.sol.size(); k++) {
                    for(int j = 0; j < sol.sol[k].size(); j++) {
                        if(sol.sol[k][j] == no) continue;
                        Point ip = instance.points[sol.sol[k][j]];
                        if(find(ip.coverageV.begin(), ip.coverageV.end(), uncovered) != ip.coverageV.end()) {
                            covered = true;
                            break;
                        }
                    }
                }
                // If not covered, stop
                if(covered) {
                    toRemove = true;
                    break;
                }
            }

            if(toRemove && sol.sol[p].size() > 3 && instance.points[no].coverageV.size() > 1) {
                sol.sol[p].erase(remove(sol.sol[p].begin(), sol.sol[p].end(), no), sol.sol[p].end());
            }
        }
    }

    return sol;
}

void perform(vector<string> filenames, vector<int> cycles, vector<double> cutoff) {
    for(int c = 0; c < cycles.size(); c++) {
        cout << endl << endl << endl;
        cout << "n cycles: " << cycles[c] << endl;
        for(int f = 0; f < filenames.size(); f++) {
            string filePath = instance_path + filenames[f];
            cout << "Reading instance from file " << filePath << endl;
            Instance instance = Instance(filePath, 4);

            cout << "Creating initial solution" << endl;
            auto start = chrono::system_clock::now();

            Solution sol;
            if(use_brkga) {
                sol = perforBRKGA(filePath, 60*60, cycles[c]);
            } else if (use_tabu_search) {
                sol = tabuSearch(5, instance, cycles[c]);
            } else {
                sol = Solution(instance, cycles[c], GIANT_TOUR);
            }

            // Prepare for coverage
            if(CHpMP) {
                sol = coverageSolution(sol, instance);
            }

            auto end = chrono::system_clock::now();
            cout << chrono::duration_cast < chrono::milliseconds>(end - start).count() << endl;

            for(int k = 0; k < sol.sol.size(); k++) {
                rotate(sol.sol[k].begin(), std::max_element(sol.sol[k].begin(), sol.sol[k].end()), sol.sol[k].end());
            }

            std::sort(sol.sol.begin(), sol.sol.end(), [&sol](const vector<int> &a, const vector<int> &b) {
                return a[0] < b[0];
            });

            cout << "Initial cost: " << sol.evaluateSolution(instance) << endl;

            double *co = &cutoff[c];
            GurobiSolver gurobiSolver = cutoff.empty() ? GurobiSolver(instance, cycles[c], &sol, NULL, filenames[f]) : GurobiSolver(instance, cycles[c], &sol, co, filenames[f]);
            sol.writeInFile(output_base_path + filenames[f] + _itos(cycles[c]) + ".txt");
            gurobiSolver.clear();
        }
    }
}

int main(int argc, char *argv[]) {
    vector<int> cycles = vector<int>();
    vector<string> filenames = vector<string>();
    vector<double> cutoff = vector<double>();

    cutoff = {};
    cycles = {4, 6, 9, 12, 16};
    filenames = {"att48.tsp"};
    perform(filenames, cycles, cutoff);

    return 0;
}
