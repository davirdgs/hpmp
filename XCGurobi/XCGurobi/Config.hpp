//
//  Config.hpp
//  XCGurobi
//
//  Created by Davi Rodrigues on 13/03/22.
//  Copyright © 2022 Qwerty. All rights reserved.
//

#ifndef Config_hpp
#define Config_hpp

#include <stdio.h>
#include <string>
using namespace std;

// Filepaths
const string instance_path = ".../XCGurobi/Instances/";
const string brkga_config_file = ".../XCGurobi/config.conf";
const string output_base_path = ".../XCGurobi/out/out_";

// Construtive heuristics - Giant Tour as default
const bool use_brkga = false;
const bool use_tabu_search = false;

// Model
const bool CHpMP = false;
const bool HpMP_M1 = true;
const bool HpMP_M2 = false;

// HpMP-M2 symmetry breaking constraints

// iyik <= sum(j) j zjk para todo i, j, k
const bool  HpMP_M2_S1 = true;

// sum(j when j > i)yjk <= min(n-1, n - 3*(p - 1))*(1 - zik[i][k])
const bool  HpMP_M2_S2 = true;

// sum(i) izik <= sum(i) izik' - (k' - k) para todo k, k', k < k'
const bool  HpMP_M2_S3 = false;

// Subtour elimination strategy - DFS as default
const bool use_monocycle = false;
const bool use_multicycle = false;


#endif /* Config_hpp */
