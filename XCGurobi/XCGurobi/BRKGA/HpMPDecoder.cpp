//
//  HpMPDecoder.cpp
//  XCGurobi
//
//  Created by Davi Rodrigues on 11/05/20.
//  Copyright © 2020 Qwerty. All rights reserved.
//


#include "HpMPDecoder.hpp"

using namespace std;
using namespace BRKGA;

//-----------------------------[ Constructor ]--------------------------------//

HpMPDecoder::HpMPDecoder(const Instance& _instance, const int _cycles):instance(_instance), cycles(_cycles) {}

//-------------------------------[ Decode ]-----------------------------------//

double HpMPDecoder::decode(Chromosome& chromosome, bool writeback) {
    Solution sol = Solution(instance, cycles, CHR_GIANT_TOUR, chromosome);
    return (double)sol.evaluateSolution(instance);
}
