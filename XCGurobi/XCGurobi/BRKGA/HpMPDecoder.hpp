//
//  HpMPDecoder.hpp
//  XCGurobi
//
//  Created by Davi Rodrigues on 11/05/20.
//  Copyright © 2020 Qwerty. All rights reserved.
//

#ifndef HpMPDecoder_hpp
#define HpMPDecoder_hpp

#include <stdio.h>
#include "chromosome.hpp"
#include "Instance.h"
#include "Solution.hpp"


class HpMPDecoder {
public:
    HpMPDecoder(const Instance& instance, const int cycles);

    /** \brief Given a chromossome, builds a solution.
     *
     * \param chromosome A vector of doubles represent a problem solution.
     * \param rewrite Indicates if the chromosome must be rewritten. Not used
     *                this decoder, but keep due to API requirements.
     * \return the cost of the solution.
     */
    double decode(BRKGA::Chromosome& chromosome, bool rewrite);

public:
    const Instance& instance;
    const int cycles;
};

#endif
