//
//  GurobiSolver.cpp
//  XCGurobi
//
//  Created by Davi Rodrigues on 18/08/19.
//  Copyright © 2019 Qwerty. All rights reserved.
//

#include "GurobiSolver.hpp"
#include <iostream>
#include <fstream>

string itos(int i) {
    stringstream s; s << i; return s.str();
}

void writeInFile(double** sol, double** yik, int cycles, int size, string filename) {
    string path = output_base_path + itos(size) + "_" + itos(cycles) + filename + ".txt";
    ofstream file;
    file.open(path, fstream::out);

    vector<vector<int>> solCycles = vector<vector<int>>();
    for(int k = 0; k < cycles; k++) {
        vector<int> c = vector<int>();
        solCycles.push_back(c);
    }

    for(int i = 0; i < size; i++) {
        for(int k = 0; k < cycles; k++) {
            if(yik[i][k] >= 0.5) {
                solCycles[k].push_back(i);
            }
        }
    }

    // order
    for(int k = 0; k < cycles; k++) {
        for(int i = 0; i < solCycles[k].size(); i++) {
            // find node that should be next to i
            for(int j = i + 1; j < solCycles[k].size(); j++) {
                if(sol[solCycles[k][i]][solCycles[k][j]] >= 0.5) {
                    int aux = solCycles[k][i+1];
                    solCycles[k][i+1] = solCycles[k][j];
                    solCycles[k][j] = aux;
                }
            }
        }
    }


    for(int k = 0; k < solCycles.size(); k++) {
        for (int i = 0; i < solCycles[k].size(); i++) {
            file << solCycles[k][i] << " ";
        }
        file << "\n";
    }
    file.close();
}

GurobiSolver::GurobiSolver(Instance _instance, int cycles, Solution *solution, double *cutoff, string filename) {
    instance = _instance;
    int n = instance.size;
    int p = cycles;

    vars = new GRBVar*[n];
    for (int i = 0; i < n; i++)
        vars[i] = new GRBVar[n];

    yik = new GRBVar*[n];
    for (int i = 0; i < n; i++)
        yik[i] = new GRBVar[p];

    env = new GRBEnv();
    GRBModel model = GRBModel(*env);

    model.set(GRB_IntParam_LazyConstraints, 1);
    model.set(GRB_DoubleParam_TimeLimit, 3600);
    if(cutoff != NULL) {
        model.set(GRB_DoubleParam_Cutoff, *cutoff);
    }

    // Create binary decision variables

    for (int i = 0; i < n; i++) {
        for (int j = 0; j <= i; j++) {
            vars[i][j] = model.addVar(0.0, 1.0, instance.distMatrix[i][j], GRB_BINARY, "x_"+itos(i)+"_"+itos(j));
            vars[j][i] = vars[i][j];
        }

        for (int k = 0; k < p; k++) {
            yik[i][k] = model.addVar(0, 1, 0, GRB_BINARY, "y_"+itos(i)+"_"+itos(k));
        }
    }

    if (CHpMP) {
        addCoverageModelConstraints(instance, &model, n, p);
    } else if (HpMP_M1) {
        addClassicModelConstraintsV1(&model, n, p);
    } else if (HpMP_M2) {
        addClassicModelConstraintsV2(&model, n, p);
        symmetryBreakingConstraintsV2(&model, n, p);
    }
    warmStart(&model, solution, vars, yik, zik, cycles);

    cb = SubtourElim(vars, yik, n, p, instance);
    model.setCallback(&cb);
    model.update();
    model.optimize();

    // Extract solution
    if (model.get(GRB_IntAttr_SolCount) == 0) {
        cout << "Could not find any solution" << endl;
        return;
    }

    double **sol = new double*[n];
    double **ySol = new double*[n];
    for (int i = 0; i < n; i++) {
        sol[i] = model.get(GRB_DoubleAttr_X, vars[i], n);
        ySol[i] = model.get(GRB_DoubleAttr_X, yik[i], cycles);
    }

    int* tour = new int[n];
    int lenA, lenB;
    int *tourA = new int[n];
    int *tourB = new int[n];

    cb.findsubtour(n, cycles, sol, ySol, lenA, tourA, lenB, tourB);

    if(lenA != 0 && lenB != 0) {
        for(int k = 0; k < p; k++) {
            if (ySol[tourA[0]][k] != ySol[tourB[0]][k]) {
                cout << "Invalid Solution! Subcycles" << endl;
            }
        }
    }

    //validate(sol, ySol, p, n);
    writeInFile(sol, ySol, cycles, n, filename);

    //cb.printSolution(sol, ySol, p, n);
    //vector<vector<vector<int>>> subtours = cb.findsubtours(n, p, sol, ySol);
    //cb.subtoursDescription(subtours);

    for (int i = 0; i < n; i++) {
        delete[] sol[i];
        delete[] ySol[i];
    }
    delete[] sol;
    delete[] tour;
    delete[] tourA;
    delete[] tourB;
}

void GurobiSolver::warmStart(GRBModel *model, Solution *solution, GRBVar** vars, GRBVar** yik, GRBVar** zik, int cycles) {
    if (solution != NULL) {
        for (int k = 0; k < cycles; k++) {
            for(int i = 0; i < solution->sol[k].size(); i++) {
                for(int j = 0; j < solution->sol[k].size(); j++) {
                    vars[i][j].set(GRB_DoubleAttr_Start, 0.0);
                    yik[i][k].set(GRB_DoubleAttr_Start, 0.0);
                    //zik[i][k].set(GRB_DoubleAttr_Start, 0.0);
                }
            }
        }

        // Cycle k
        for (int k = 0; k < cycles; k++) {
            //zik[solution->sol[k][0]][k].set(GRB_DoubleAttr_Start, 1.0);

            // Node i in cycle k
            for(int i = 0; i < solution->sol[k].size(); i++) {

                vector<int> kCycle = solution->sol[k];
                yik[kCycle[i]][k].set(GRB_DoubleAttr_Start, 1.0);

                if(i > 0) {
                    vars[kCycle[i]][kCycle[i-1]].set(GRB_DoubleAttr_Start, 1.0);
                } else if(i == 0) {
                    vars[kCycle[0]][kCycle[kCycle.size() - 1]].set(GRB_DoubleAttr_Start, 1.0);
                }
            }
        }
    }
}

void GurobiSolver::addCoverageModelConstraints(Instance instance, GRBModel *model, int n, int p) {
        // sum(xij) >= 2 -> j cobre i (3)
//    for (int i = 0; i < n; i++) {
//        GRBLinExpr expr = 0;
//        vector<int> cover(instance.points[i].coverage.begin(), instance.points[i].coverage.begin() + coveragek);
//        for(int k = 0; k < coveragek; k++) {
//            for(int j = 0; j < n; j++) {
//                // j cover i
//                if(std::find(cover.begin(), cover.end(), j) != cover.end()) continue;
//                expr += vars[j][cover[k]];
//            }
//        }
//        model->addConstr(expr >= 2, "deg3_"+itos(i));
//    }

    // Pelo menos um nó que cobre i em algum ciclo
    for (int i = 0; i < n; i++) {
        GRBLinExpr expr = 0;
        for (int k = 0; k < p; k++) {
            for(int j = 0; j < n; j++) {
                // Nós que cobrem i
                vector<int> vi = instance.points[i].coverageV;
                // j cover i
                if(std::find(vi.begin(), vi.end(), j) != vi.end()) {
                    expr += yik[j][k];
                }
            }
        }
        model->addConstr(expr >= 1, "dep4_"+itos(i));
    }

    // Se i pertence a um ciclo, seu grau é dois
    for (int i = 0; i < n; i++) {
        GRBLinExpr expr = 0;
        GRBLinExpr exprY = 0;
        for (int j = 0; j < n; j++)
            expr += vars[i][j];
        for (int k = 0; k < p; k++)
            exprY += yik[i][k];
        model->addConstr(expr == 2 * exprY, "dep1_"+itos(i));
    }

    // Nó pode pertencer a no máximo um caminho
    for (int i = 0; i < n; i++) {
        GRBLinExpr expr = 0;
        for (int k = 0; k < p; k++)
            expr += yik[i][k];
        model->addConstr(expr <= 1, "dep2_"+itos(i));
    }

    //loop constraint
    for (int i = 0; i < n; i++) {
        GRBLinExpr expr = vars[i][i];
        model->addConstr(expr == 0, "loop_"+itos(i));
    }

    // Se dois nós estãoconectados, eles pertencem ao mesmo ciclo
    for (int k = 0; k < p; k++) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j <= i; j++) {
                if (i != j) {
                    GRBLinExpr a = yik[i][k] + vars[i][j];
                    GRBLinExpr b = yik[j][k] + vars[i][j];
                    GRBLinExpr ra = 1 + yik[j][k];
                    GRBLinExpr rb = 1 + yik[i][k];
                    model->addConstr(a, GRB_LESS_EQUAL, ra);
                    model->addConstr(b, GRB_LESS_EQUAL, rb);
                }
            }
        }
    }

    // At least 3 nodes in each cycle
    for (int k = 0; k < p; k++) {
        GRBLinExpr sum = 0;
        for (int i = 0; i < n; i++) {
            sum += yik[i][k];
        }
        model->addConstr(sum >= 3, "ciclemin_"+itos(k));
    }
}

void GurobiSolver::addClassicModelConstraintsV1(GRBModel *model, int n, int p) {
    //loop constraint
    for (int i = 0; i < n; i++) {
        GRBLinExpr expr = vars[i][i];
        model->addConstr(expr == 0, "loop_"+itos(i));
    }

    for (int k = 0; k < p; k++) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j <= i; j++) {
                if (i != j) {
                    GRBLinExpr a = yik[i][k] + vars[i][j];
                    GRBLinExpr b = yik[j][k] + vars[i][j];
                    GRBLinExpr ra = 1 + yik[j][k];
                    GRBLinExpr rb = 1 + yik[i][k];
                    model->addConstr(a, GRB_LESS_EQUAL, ra);
                    model->addConstr(b, GRB_LESS_EQUAL, rb);
                }
            }
        }
    }

    // At least 3 nodes in each cycle
    for (int k = 0; k < p; k++) {
        GRBLinExpr sum = 0;
        for (int i = 0; i < n; i++) {
            sum += yik[i][k];
        }
        model->addConstr(sum >= 3, "ciclemin_"+itos(k));
    }

    // Degree-2 constraints
    for (int i = 0; i < n; i++) {
        GRBLinExpr expr = 0;
        for (int j = 0; j < n; j++)
            expr += vars[i][j];
        model->addConstr(expr == 2, "deg2_"+itos(i));
    }

    // Depots constraints
    for (int i = 0; i < n; i++) {
        GRBLinExpr sum = 0;
        for (int k = 0; k < p; k++) {
            sum += yik[i][k];
        }
        model->addConstr(sum == 1, "dep_"+itos(i));
    }
}

void GurobiSolver::symmetryBreakingConstraintsV1(GRBModel *model, int n, int p) {
    // eliminação de simetrias -> sum y[i][k] <= sum y[i][k+1]
    for (int k = 1; k < p; k++) {
        GRBLinExpr sumA = 0;
        GRBLinExpr sumB = 0;
        for (int i = 0; i < n; i++) {
            sumA += yik[i][k-1];
            sumB += yik[i][k];
        }
        model->addConstr(sumA, GRB_LESS_EQUAL, sumB);
    }

    // eliminação de simetrias -> sum(0 a p) y[i][k] = 1
    for (int k = 0; k < p; k++) {
        GRBLinExpr sumA = 0;
        for(int l = 0; l <= k; l++) {
            sumA += yik[k][l];
        }
        model->addConstr(sumA == 1, "sim_"+itos(k));
    }


    // eliminação de simetrias -> sum i*y[i][k] <= sum (i+1)y[i][k+1]
    for (int k = 1; k < p; k++) {
        GRBLinExpr sumA = 0;
        GRBLinExpr sumB = 0;
        for (int i = 0; i < n; i++) {
            sumA += i*yik[i][k-1];
            sumB += i*yik[i][k];
        }
        model->addConstr(sumA, GRB_LESS_EQUAL, sumB);
    }
}

void GurobiSolver::symmetryBreakingConstraintsV2(GRBModel *model, int n, int p) {
    // iyik <= sum(j) j zjk para todo i, j, k (1)
    if(HpMP_M2_S1) {
        for (int k = 0; k < p; k++) {
            for (int i = 0; i < n; i++) {
                GRBLinExpr sum = 0;
                for (int j = 0; j < n; j++) {
                    sum += j * zik[j][k];
                }
                model->addConstr(i*yik[i][k], GRB_LESS_EQUAL, sum);
            }
        }
    }

    // sum(j when j > i)yjk <= min(n-1, n - 3*(p - 1))*(1 - zik[i][k])
    if(HpMP_M2_S2) {
        for (int k = 0; k < p; k++) {
            for (int i = 0; i < n; i++) {
                GRBLinExpr sum = 0;
                for (int j = i + 1; j < n; j++) {
                    sum += yik[j][k];
                }
                GRBLinExpr exp = min(n-1, n - 3*(p - 1))*(1 - zik[i][k]);
                model->addConstr(sum, GRB_LESS_EQUAL, exp);
            }
        }
    }

    // sum(i) izik <= sum(i) izik' - (k' - k) para todo k, k', k < k' (3)
    if(HpMP_M2_S3) {
        for (int k = 0; k < p; k++) {
            for (int kl = k + 1; kl < p; kl++) {
                GRBLinExpr sumA = 0;
                GRBLinExpr sumB = 0;
                for (int i = 0; i < n; i++) {
                    sumA += i*zik[i][k];
                    sumB += i*zik[i][kl];
                }
                model->addConstr(sumA, GRB_LESS_EQUAL, sumB - (kl - k));
            }
        }
    }
}

void GurobiSolver::addClassicModelConstraintsV2(GRBModel *model, int n, int p) {
    // Create z binary decision variables
    zik = new GRBVar*[n];
    for (int i = 0; i < n; i++) {
        zik[i] = new GRBVar[p];
        for (int k = 0; k < p; k++) {
            zik[i][k] = model->addVar(0, 1, 0, GRB_BINARY, "z_"+itos(i)+"_"+itos(k));
        }
    }

    // Degree-2 constraints
    for (int i = 0; i < n; i++) {
        GRBLinExpr expr = 0;
        for (int j = 0; j < n; j++) {
            if (i != j) {
                expr += vars[i][j];
            }
        }
        model->addConstr(expr == 2, "deg2_"+itos(i));
    }

    // At least 3 nodes in each cycle
    for (int k = 0; k < p; k++) {
        GRBLinExpr sum = 0;
        for (int i = 0; i < n; i++) {
            sum += yik[i][k];
        }
        model->addConstr(sum >= 3, "ciclemin_"+itos(k));
    }

    // All nodes in a cycle
    for (int i = 0; i < n; i++) {
        GRBLinExpr sum = 0;
        for (int k = 0; k < p; k++) {
            sum += yik[i][k];
        }
        model->addConstr(sum == 1, "dep_"+itos(i));
    }

    // loop constraint
    for (int i = 0; i < n; i++) {
        GRBLinExpr expr = vars[i][i];
        model->addConstr(expr == 0, "loop_"+itos(i));
    }

    for (int k = 0; k < p; k++) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j <= i; j++) {
                if (i != j) {
                    GRBLinExpr a = yik[i][k] + vars[i][j];
                    GRBLinExpr b = yik[j][k] + vars[i][j];
                    GRBLinExpr ra = 1 + yik[j][k];
                    GRBLinExpr rb = 1 + yik[i][k];
                    model->addConstr(a, GRB_LESS_EQUAL, ra);
                    model->addConstr(b, GRB_LESS_EQUAL, rb);
                }
            }
        }
    }

    // z constraints
    for (int k = 0; k < p; k++) {
        GRBLinExpr sum = 0;
        for (int i = 0; i < n; i++) {
            sum += zik[i][k];
        }
        model->addConstr(sum == 1, "zdep_"+itos(k));
    }

    // redundant
//    for (int i = 0; i < n; i++) {
//        GRBLinExpr sum = 0;
//        for (int k = 0; k < p; k++) {
//            sum += zik[i][k];
//        }
//        model->addConstr(sum <= 1, "zrdep_"+itos(i));
//    }

    // yik >= zik for all i, k
    for (int i = 0; i < n; i++) {
        for (int k = 0; k < p; k++) {
            model->addConstr(zik[i][k], GRB_LESS_EQUAL, yik[i][k]);
        }
    }
}

void GurobiSolver::validate(double** sol, double** yik, int cycles, int size) {
    
    vector<vector<int>> solCycles = vector<vector<int>>();
    for(int k = 0; k < cycles; k++) {
        vector<int> c = vector<int>();
        solCycles.push_back(c);
    }
    
    // find loops
    for (int i = 0; i < size; i++) {
        if(sol[i][i] > 0.5) {
            cout << "Invalid Solution: loop node " << i << endl;
            return;
        }
    }
    
    // Check if all nodes are associated to only one cycle
    for(int i = 0; i < size; i++) {
        bool setted = false;
        for(int k = 0; k < cycles; k++) {
            if(yik[i][k] == 1) {
                if(setted) {
                    cout << "Invalid Solution! Node in multiple cycles" << endl;
                    return;
                } else {
                    setted = true;
                    solCycles[k].push_back(i);
                }
            }
        }
        if(!setted) {
            cout << "Invalid Solution! Node without cycle" << endl;
            return;
        }
    }
    
    // 3 nodes per cycle at least; all nodes in cycles
    unsigned long count = 0;
    for(int k = 0; k < solCycles.size(); k++) {
        if(solCycles[k].size() < 3) {
            cout << "Invalid Solution! Cycle with less than three nodes" << endl;
            return;
        }
        count = count + solCycles[k].size();
    }
    
    if(count != size) {
        cout << "Invalid Solution! Node without cycle" << endl;
        return;
    }
    
    // Symetric solution
    for(int i = 0; i < size; i++) {
        double countI = 0;
        for(int j = 0; j < size; j++) {
            countI = countI + sol[i][j];
            if(sol[i][j] != sol[j][i]) {
                cout << "Invalid Solution! Asymetric solution" << endl;
                return;
            }
        }
        if(countI != 2) {
            cout << "Invalid Solution! Node degree not two" << endl;
            return;
        }
    }
    
    cout << "Valid Solution!" << endl;
}

void GurobiSolver::clear() {
    for (int i = 0; i < instance.size; i++) {
        delete[] vars[i];
        delete[] yik[i];
    }
    delete[] vars;
    delete env;
}
