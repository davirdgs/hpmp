//
//  GurobiSolver.hpp
//  XCGurobi
//
//  Created by Davi Rodrigues on 18/08/19.
//  Copyright © 2019 Qwerty. All rights reserved.
//

#ifndef GurobiSolver_hpp
#define GurobiSolver_hpp

#include <stdio.h>
#include "Instance.h"
#include "SubtourElim.hpp"
#include "Solution.hpp"
#include "Config.hpp"
#include <sstream>

class GurobiSolver {
public:
    Instance instance;
    int cycles;
    GurobiSolver(Instance instance, int cycles, Solution *solution, double *cutoff, string filename);
    void addClassicModelConstraintsV1(GRBModel *model, int n, int p);
    void addClassicModelConstraintsV2(GRBModel *model, int n, int p);
    void addCoverageModelConstraints(Instance instance, GRBModel *model, int n, int p);
    void symmetryBreakingConstraintsV1(GRBModel *model, int n, int p);
    void symmetryBreakingConstraintsV2(GRBModel *model, int n, int p);
    void warmStart(GRBModel *model, Solution *solution, GRBVar** vars, GRBVar** yik, GRBVar** zik, int cycles);
    void validate(double** sol, double** yik, int cycles, int size);
    void clear();
    
    GRBEnv *env;
    GRBVar **vars;
    GRBVar **yik;
    GRBVar **zik;
    SubtourElim cb;
};

#endif /* GurobiSolver_hpp */
