//
//  SubtourElim.cpp
//  XCGurobi
//
//  Created by Davi Rodrigues on 18/08/19.
//  Copyright © 2019 Qwerty. All rights reserved.
//

#include "SubtourElim.hpp"
#include <lemon/list_graph.h>
#include <lemon/gomory_hu.h>
#include "Config.hpp"

using namespace std;
using namespace lemon;

bool verbose = false;

ListGraph graph;
vector<ListGraph::Node> nodes;
vector<ListGraph::Edge> edges;
ListGraph::EdgeMap<double> capacity(graph);

SubtourElim::SubtourElim() { }

SubtourElim::SubtourElim(GRBVar** xvars, GRBVar** xyik, int xn, int xp, Instance xinstance) {
    vars = xvars;
    yik = xyik;
    n    = xn;
    p = xp;
    instance = xinstance;
}

void SubtourElim::callback() {
    try {
        if (where == GRB_CB_MIPSOL) {
            // Found an integer feasible solution - does it visit every node?
            double **x = new double*[n];
            double **y = new double*[n];
            int i;
            for (i = 0; i < n; i++) {
                x[i] = getSolution(vars[i], n);
                y[i] = getSolution(yik[i], p);
            }
            
            addSubtourEliminationConstraint(x, y);
            
            for (i = 0; i < n; i++) {
                delete[] x[i];
                delete[] y[i];
            }
            delete[] x;
            delete[] y;
            
        } else if (where == GRB_CB_MIPNODE && getIntInfo(GRB_CB_MIPNODE_STATUS) == GRB_OPTIMAL && false) {//} && getDoubleInfo(GRB_CB_MIPNODE_NODCNT) < 25) {

            double **x = new double*[n];
            double **y = new double*[n];

            for (int i = 0; i < n; i++) {
                x[i] = getNodeRel(vars[i], n);
                y[i] = getNodeRel(yik[i], p);
            }

            if (use_monocycle) {
                addCutWithGHTMonocycle(x, y);
            } else if (use_multicycle) {
                addCutWithGHTMulticycle(x, y);
            } else {
                addCutWithDFS(x, y);
            }

        }

    } catch (GRBException e) {
        cout << "Error number: " << e.getErrorCode() << endl;
        cout << e.getMessage() << endl;
    } catch (...) {
        cout << "Error during callback" << endl;
    }
}

//            Para cada aresta da árvore GH e : 1 até V-1
//            minCut = GH<e>
//            // recolher as arestas do corte
//            Para cada nó do lado esquerdo do corte v_{esq} : 1 até S_{esq}
//                 Para cada nó do lado direto do corte v_{dir} : 1 até S_{dir}
//
//            // recolher quais serão os nós "i" e "j" referentes às variáveis "yi,k" e "yj,k"
//            Para cada ciclo k : 1 até p
//                 // recolher os nós "i" às variáveis "yi,k" para o lado esquerdo
//                 Para cada nó do lado esquerdo do corte v_{esq} : 1 até S_{esq}
//                 // recolher os nós "j" às variáveis "yj,k" para o lado direito
//                 Para cada nó do lado direto do corte v_{dir} : 1 até S_{dir}
void SubtourElim::addCutWithGHTMulticycle(double **x, double **y) {
    prepareGraph(x);
    GomoryHu<ListGraph, ListGraph::EdgeMap<double>> gHT(graph, capacity);
    gHT.run();

    // For each gH node
    for(int i = 0; i < instance.points.size(); i++) {
        ListGraph::Node nodej = gHT.predNode(nodes[i]);
        if (nodej == INVALID) continue;

        GRBLinExpr expr = 0;
        GRBLinExpr yjkc = 0;
        GRBLinExpr yikc = 0;

        int kCount = 0;
        vector<int> leftNodes = vector<int>();
        vector<int> rightNodes = vector<int>();

        if (verbose) cout << endl << endl << "left nodes" << endl;
        for(GomoryHu<ListGraph, ListGraph::EdgeMap<double>>::MinCutNodeIt nd(gHT, nodes[i], nodej, true); nd!=INVALID; nd++) {
            int nodeId = graph.id(nd);
            int nodeIndex = -1;

            for(int l = 0; l < nodes.size(); l++) {
                if (graph.id(nodes[l]) == nodeId) {
                    nodeIndex = l;
                }
            }

            assert(nodeIndex != -1);
            leftNodes.push_back(nodeIndex);
        }

        for(GomoryHu<ListGraph, ListGraph::EdgeMap<double>>::MinCutNodeIt nd(gHT, nodes[i], nodej, false); nd!=INVALID; nd++) {
            int nodeId = graph.id(nd);
            int nodeIndex = -1;

            for(int l = 0; l < nodes.size(); l++) {
                if (graph.id(nodes[l]) == nodeId) {
                    nodeIndex = l;
                }
            }

            assert(nodeIndex != -1);
            rightNodes.push_back(nodeIndex);
        }

        // iterate over edges
        if (verbose) cout << endl << endl << "Cut edges" << endl;
        double minCut = 0;
        for(int v = 0; v < leftNodes.size(); v++) {
            for(int u = 0; u < rightNodes.size(); u++) {
                expr += vars[leftNodes[v]][rightNodes[u]];
                minCut += x[leftNodes[v]][rightNodes[u]];
            }
        }

        if(minCut > 2) continue;

        // For each cycle
        double sumMaxY = 0;
        for(int k = 0; k < p; k++) {

            double maxyi = -1;
            double maxyj = -1;
            int iIndex = -1;
            int jIndex = -1;

            for(int v = 0; v < leftNodes.size(); v++) {
                if(y[leftNodes[v]][k] > maxyi) {
                    maxyi = y[leftNodes[v]][k];
                    iIndex = leftNodes[v];
                }
            }

            for(int u = 0; u < rightNodes.size(); u++) {
                if(y[rightNodes[u]][k] > maxyj) {
                    maxyj = y[rightNodes[u]][k];
                    jIndex = rightNodes[u];
                }
            }

            if (maxyj + maxyi >= 1) {
                kCount++;
                sumMaxY += maxyi + maxyj;
                yikc += yik[iIndex][k];
                yjkc += yik[jIndex][k];
            }
        }

        if(kCount > 0) {
            addCut(expr >= 2*(yikc + yjkc - kCount));
        } // end of if(kCount > 0)
    }

    for (int i = 0; i < n; i++) {
        delete[] x[i];
        delete[] y[i];
    }
    delete[] x;
    delete[] y;
}

void SubtourElim::addCutWithGHTMonocycle(double **x, double **y) {
    prepareGraph(x);
    GomoryHu<ListGraph, ListGraph::EdgeMap<double>> gHT(graph, capacity);
    gHT.run();

    // For each cycle
    for(int k = 0; k < p; k++) {
        // For each node pair
        for(int i = 0; i < instance.points.size(); i++) { // verificar o valor de y[i][k] antes de entrar no laço
            if(y[i][k] == 0) continue;
            for(int j = i + 1; j < instance.points.size(); j++) {
                double minCut = gHT.minCutValue(nodes[i], nodes[j]);
                if(y[i][k] + y[j][k] <= 1 || minCut >= 2*(y[i][k] + y[j][k] - 1)) {
                    continue;
                }

                GRBLinExpr expr = 0;
                for(GomoryHu<ListGraph, ListGraph::EdgeMap<double>>::MinCutNodeIt nd(gHT, nodes[i], nodes[j], true); nd!=INVALID; nd++) {
                    int leftNodeIndex = graph.id(nd);
                    for(GomoryHu<ListGraph, ListGraph::EdgeMap<double>>::MinCutNodeIt nd(gHT, nodes[i], nodes[j], false); nd!=INVALID; nd++) {
                        int rightNodeIndex = graph.id(nd);
                        expr += vars[leftNodeIndex][rightNodeIndex];
                    }
                }

                addCut(expr >= 2*(yik[i][k] + yik[j][k] - 1));
            }
        }// For each node pair
    }// For each cycle

    for (int i = 0; i < n; i++) {
        delete[] x[i];
        delete[] y[i];
    }
    delete[] x;
    delete[] y;
}

void SubtourElim::addCutWithDFS(double **x, double **y) {
    //vector<vector<int>> components = findPartialComponent(n, p, x, y, 0.0);
    vector<vector<int>> components = findComponentsWithBisection(n, p, x, y);
    //cout << components.size() << endl;

    for(int c = 0; c < components.size(); c++) {
        int yikc = 0;
        int yjkc = 0;
        int kc = 0;
        GRBLinExpr expr = 0;

        for(int k = 0; k < p; k++) {
            int maxyi = 0; //maior valor de yik para nós i pertencentes a S
            int maxyj = 0; //maior valor de yik para nós j pertencentes a V - S
            vector<int> left = vector<int>();
            vector<int> right = vector<int>();

            for(int i = 0; i < n; i++) {
                if(find(components[c].begin(), components[c].end(), i) != components[c].end()) { // i pertence a S
                    left.push_back(i);
                    if(maxyi < y[i][k]) {
                        maxyi = y[i][k];
                    }
                } else { // i pertence a V - S
                    right.push_back(i);
                    if(maxyj < y[i][k]) {
                        maxyj = y[i][k];
                    }
                }
            }

            // Calcular corte
            for(int i = 0; i < left.size(); i++) {
                for(int j = 0; j < right.size(); j++) {
                    expr += vars[left[i]][right[j]];
                }
            }

            if(maxyi + maxyj >= 1) {
                kc++;
                yikc += maxyi;
                yjkc += maxyj;
            }
        }
        if(kc > 0) {
            addCut(expr >= 2*(yikc + yjkc - kc));
        }
    }
}

void SubtourElim::prepareGraph(double **x) {
    graph.clear();
    nodes.clear();
    nodes = vector<ListGraph::Node>();
    for(int i = 0; i < instance.points.size(); i++) {
        ListGraph::Node node = graph.addNode();
        nodes.push_back(node);
    }

    for(int i = 0; i < instance.points.size(); i++) {
        for(int j = i+1; j < instance.points.size(); j++) {
            ListGraph::Edge edge = graph.addEdge(nodes[i], nodes[j]);
            edges.push_back(graph.addEdge(nodes[i], nodes[j]));
            capacity[edge] = x[i][j];
        }
    }
}

vector<vector<vector<int>>> subtours = vector<vector<vector<int>>>();

void SubtourElim::addSubtourEliminationConstraint(double** x, double** y) {
    subtours.clear();
    subtours = findsubtours(n, p, x, y);

    // Add subtour elimination constraint

    for(int k = 0; k < subtours.size(); k++) {
        if(subtours[k].size() == 1) continue; // one subcycle per cycle
        for(int ci = 0; ci < subtours[k].size(); ci++) {
            for(int cj = ci + 1; cj < subtours[k].size(); cj++) {
                GRBLinExpr expr = 0;
                for(int ei = 0; ei < subtours[k][ci].size(); ei++) {
                    for(int ej = 0; ej < subtours[k][cj].size(); ej++) {
                        expr += vars[subtours[k][ci][ei]][subtours[k][cj][ej]];
                    }
                }
                addLazy(expr >= 2*(yik[subtours[k][ci][0]][k] + yik[subtours[k][cj][0]][k] - 1));
                return;
            }
        }
    }
}

vector<int> tour = vector<int>();
void SubtourElim::findsubtour(int n, int p, double** sol, double** ySol, int& tourlenA, int* tourA, int& tourlenB, int* tourB) {
    bool* unvisited = new bool[n];
    tour.clear();
    int i, k, node, len, start;
    
    vector<vector<int>> pCycles;
    for(i = 0; i < p; i++) {
        vector<int> list;
        pCycles.push_back(list);
    }
    
    for (i = 0; i < n; i++)
        unvisited[i] = false;
    
    start = 0;
    node = 0;
    while (start < n) {
        for (node = 0; node < n; node++)
            if (!unvisited[node])
                break;
        if (node == n)
            break;
        for (len = 0; len < n; len++) {
            tour.push_back(node);
            unvisited[node] = true;
            for (i = 0; i < n; i++) {
                if (sol[node][i] > 0.5 && !unvisited[i]) {
                    node = i;
                    break;
                }
            }
            if (i == n) {
                len++;
                start += len;
                break;
            }
        }
        if (!tour.empty()) {
            for(k = 0; k < p; k++) {
                if(ySol[tour[0]][k] > 0.5) {
                    if(!pCycles[k].empty()) {
                        tourlenA = int(tour.size());
                        tourlenB = int(pCycles[k].size());
                        for(i = 0; i < tour.size(); i++) {
                            tourA[i] = tour[i];
                        }
                        for(i = 0; i < pCycles[k].size(); i++) {
                            tourB[i] = pCycles[k][i];
                        }
                        
                        // Clean pCycles
                        for (i = 0; i < p; i++) {
                            pCycles[i].clear();
                        }
                        pCycles.clear();
                        tour.clear();
                        
                        return;
                    } else {
                        pCycles[k] = tour;
                    }
                    break;
                }
            }
        }
    }
    
    // Clean pCycles
    for (i = 0; i < p; i++) {
        pCycles[i].clear();
    }
    pCycles.clear();
    tour.clear();
    delete[] unvisited;
}

void SubtourElim::printComponents(vector<vector<int>> components) {
    cout << "components: " << components.size() << endl;
    for(int c = 0; c < components.size(); c++) {
        cout << "componente " << c << ": ";
        for(int i = 0; i < components[c].size(); i++) {
            cout << components[c][i] << " ";
        }
        cout << endl;
    }
}

vector<vector<int>> SubtourElim::findComponentsWithBisection(int n, int p, double** sol, double** ySol) {
    double upLimiar = 0.5;
    double lowLimiar = 0.0;
    vector<vector<int>> components = findPartialComponent(n, p, sol, ySol, upLimiar);

    // Find min limiar that creates more than one component
    while (components.size() == 1 && upLimiar <= 1) {
        upLimiar += 0.1;
        components = findPartialComponent(n, p, sol, ySol, upLimiar);
    }

    while (findPartialComponent(n, p, sol, ySol, lowLimiar).size() == 1 && (upLimiar - lowLimiar > 0.001)) {
        double newLimiar = (upLimiar + lowLimiar) / 2;
        if (findPartialComponent(n, p, sol, ySol, newLimiar).size() > 1) {
            upLimiar = newLimiar;
        } else {
            lowLimiar = newLimiar;
        }
    }

    vector<vector<int>> lowComponents = findPartialComponent(n, p, sol, ySol, lowLimiar);

    if(lowLimiar > 0) {
        cout << "low limiar: " << lowLimiar << "  components low: " << lowComponents.size() << endl;
        cout << "up limiar: " << upLimiar << "  components low: " << findPartialComponent(n, p, sol, ySol, upLimiar).size() << endl;
    }

    if (lowComponents.size() > 1) {
        return lowComponents;
    } else {
        return findPartialComponent(n, p, sol, ySol, upLimiar);
    }
}

vector<vector<int>> SubtourElim::findPartialComponent(int n, int p, double** sol, double** ySol, double limiar) {
    bool* visited = new bool[n];
    int start = 0;

    for(int i = 0; i < n; i++) {
        visited[i] = false;
    }

    vector<vector<int>> components = vector<vector<int>>();

    while (start < n) {
        //enquanto houver algum nó não visitado
        if(visited[start]) {
            start++;
            continue;
        }

        //defina pilha com todos os nós não visitados
        list<int> unvisitedNodes = list<int>();
        for(int i = 0; i < n; i++) {
            if(!visited[i]) {
                unvisitedNodes.push_back(i);
            }
        }

        //defina componente
        vector<int> component = vector<int>();

        //enquanto pilha não estiver vazia:
        while(!unvisitedNodes.empty()) {
            //defina próximo como o primeiro nó de pilha
            int next = unvisitedNodes.front();
            //remova próximo de pilha
            unvisitedNodes.pop_front();

            //adiciona próximo a componente
            if(component.empty()) {
                component.push_back(next);
                visited[next] = true;
            }

            //para cada nó em nós:
            bool findConnection = false;
            for(int i = 0; i < n; i++) {
                //se nó não foi visitado e aresta entre nó e próximo > limiar:
                if(!visited[i] && sol[i][next] > limiar) {
                    //adiciona nó na pilha
                    unvisitedNodes.push_back(i);
                    //adiciona nó na componente
                    component.push_back(i);
                    visited[i] = true;
                    findConnection = true;
                }
            }
            //se nenhuma conexão foi encontrada, interrompe laço:
            if(!findConnection) break;
        }
        components.push_back(component);
        start = 0;
    }


    delete[] visited;
    return components;
}

vector<vector<vector<int>>> SubtourElim::findsubtours(int n, int p, double** sol, double** ySol) {
    bool* visited = new bool[n];
    
    for(int i = 0; i < n; i++) {
        visited[i] = false;
    }
    
    vector<vector<vector<int>>> subtours = vector<vector<vector<int>>>();
    
    for(int k = 0; k < p; k++) {
        subtours.push_back(vector<vector<int>>());
    }
    
    for(int next = 0; next < n; next++) {
        // Add cycles node by node
        if(visited[next]) continue;
        
        for (int k = 0; k < p; k++) {
            if(ySol[next][k] > 0.5) {
                // New subcycle
                subtours[k].push_back(vector<int>());
                subtours[k][subtours[k].size() - 1].push_back(next);
                visited[next] = true;
                
                // Find connected nodes to next
                
                list<int> stack = list<int>();
                stack.push_back(next);
                
                while (!stack.empty()) {
                    int incumbent = stack.front();
                    stack.pop_front();
                    
                    for(int i = 0; i < n; i++) {
                        if(visited[i]) continue;
                        if(sol[incumbent][i] > 0.5) {
                            stack.push_back(i);
                            subtours[k][subtours[k].size() - 1].push_back(i);
                            visited[i] = true;
                        }
                    }
                }
                
            }
        }
    }
    
    delete[] visited;
    return subtours;
}

void SubtourElim::partialResultDescription(double** x, double**  y) {
    cout << endl << endl << "printing edges (X)" << endl << endl;
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            cout << "x[" << i << "][" << j << "] = " << x[i][j] << endl;
        }
    }

    cout << endl << endl << "printing nodes cycles (Y)" << endl << endl;
    for (int i = 0; i < n; i++) {
        for (int a = 0; a < p; a++) {
            cout << "y[" << i << "][" << a << "] = " << y[i][a] << endl;
        }
    }
}

void SubtourElim::subtoursDescription(vector<vector<vector<int>>> subtours) {
    cout << endl << endl << "Subtours Description" << endl << endl;
    cout << subtours.size() << " cycles" << endl << endl;
    
    for(int k = 0; k < subtours.size(); k++) {
        cout << endl << "Cycle " << k << endl;
        cout << "Subcycles " << subtours[k].size() << endl;
        
        for(int c = 0; c < subtours[k].size(); c++){
            cout << "Subcycle " << c << " size: " << subtours[k][c].size() << endl;
            
            for(int i = 0; i < subtours[k][c].size(); i++) {
                cout << subtours[k][c][i] << " ";
            }
            cout << endl;
        }
    }
}

void SubtourElim::printSolution(double** sol, double** yik, int cycles, int size) {
    cout << endl << endl << "Printing xij" << endl << endl;
    cout << "   ";
    for(int i = 0; i < size; i++) {
        cout << i << " ";
    }
    cout << endl;
    for(int i = 0; i < size; i++) {
        cout << i << ": ";
        for(int j = 0; j < size; j++) {
            cout << sol[i][j] << " ";
        }
        cout << endl;
    }
    
    cout << endl << endl << "Printing yik" << endl << endl;
    
    for(int k = 0; k < cycles; k++) {
        for(int i = 0; i < size; i++) {
            cout << yik[i][k] << " ";
        }
        cout << endl;
    }
    
    cout << endl << endl;
}
