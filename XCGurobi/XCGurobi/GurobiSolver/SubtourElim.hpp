//
//  SubtourElim.hpp
//  XCGurobi
//
//  Created by Davi Rodrigues on 18/08/19.
//  Copyright © 2019 Qwerty. All rights reserved.
//

#ifndef SubtourElim_hpp
#define SubtourElim_hpp

#include <stdio.h>
#include "gurobi_c++.h"
#include "Instance.h"
#include <lemon/list_graph.h>
#include <lemon/gomory_hu.h>

class SubtourElim: public GRBCallback {
    
public:
    GRBVar** vars;
    GRBVar** yik;
    int n;
    int p;
    
    Instance instance;
    
    SubtourElim();
    SubtourElim(GRBVar** xvars, GRBVar** xyik, int xn, int xp, Instance xinstance);
    void findsubtour(int n, int p, double** sol, double** ySol, int& tourlenA, int* tourA, int& tourlenB, int* tourB);
    vector<vector<vector<int>>> findsubtours(int n, int p, double** sol, double** ySol);
    vector<vector<int>> findPartialComponent(int n, int p, double** sol, double** ySol, double limiar);
    vector<vector<int>> findComponentsWithBisection(int n, int p, double** sol, double** ySol);
    void addCutWithDFS(double **x, double **y);
    void addCutWithGHTMulticycle(double **x, double **y);
    void addCutWithGHTMonocycle(double **x, double **y);
    void printComponents(vector<vector<int>> components);
    void printSolution(double** sol, double** yik, int cycles, int size);
    void subtoursDescription(vector<vector<vector<int>>> subtours);
    void partialResultDescription(double** x, double**  y);
    void addSubtourEliminationConstraint(double** x, double** y);
    void prepareGraph(double **x);
    void callback();
};

#endif /* SubtourElim_hpp */
