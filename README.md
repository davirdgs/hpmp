# README #

Este repositório contém o código fonte e ensaios referentes à dissertação de mestrado "Formulações e Desigualdades Válidas para o Problema das P-Medianas Hamiltonianas" - Unicamp - 2022


### Conteúdo ###

* Pasta "XCGurobi" contendo projeto XCode em C++ para a execução de metaheurísticas e modelos exatos para o HpMP e CHpMP.
* Pasta "Ensaios", contendo resultados computacionais apresentados na dissertação.

### Configuração ###

As configurações de execução se encontram no arquivo Config.hpp:

* instance\_path: Caminho para o folder contendo instâncias do TSPLib. Apenas instâncias com NODE\_COORD\_SECTION são suportadas.
* brkga\_config\_file: Caminho para o arquivo de execução do BRKGA, somente necessário se o BRKGA for escolhido para geração de soluções de partida.
* output_base_path: Caminho para folder onde soluções dos ensaios serão salvas.
* use\_brkga: BRKGA é utilizado para geração das soluções de partida.
* use\_tabu\_search: Busca tabu é utilizada para geração das soluções de partida. Se ambos use\_brkga e use\_tabu\_search forem falsos, as soluções de partida serão geradas utilizando o Giant Tour Heuristic.
* CHpMP: Executa modelo do HpMP com cobertura.
* HpMP_M1: Executa modelo HpMP-M1 descrito na dissertação.
* HpMP_M2: Executa modelo HpMP-M2 descrito na dissertação.
* HpMP\_M2\_S1, HpMP\_M2\_S2, HpMP\_M2\_S3: Inclui restrições de eliminação de simetria para o modelo HpMP-M2.
* use\_monocycle: Utiliza a estratégia de eliminação de subciclos ilegais de ciclo único descrito na dissertação.
* use\_multicycle: Utiliza a estratégia de eliminação de subciclos ilegais de múltiplos ciclos descrito na dissertação. Se ambos use\_monocycle e use\_multicycle forem falsos, a estratégia utilizada será DFS.

Nota: Uma licença ativa do Gurobi é necessária para a execução.

### Contato ###

* Davi Rodrigues - davirdgs@gmail.com